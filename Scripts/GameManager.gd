extends Node

var highscore = 0
var gameScene = load("res://Gameplay.tscn")

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("Button").connect("pressed",self,"StartGame");
	pass

func StartGame():
	if (get_node("GameHolder").get_child_count() == 0):
		var cScene = gameScene.instance();
		get_node("GameHolder").add_child(cScene);
		cScene.connect("GameOverUpdateScore",self,"UpdateScore")
		
func UpdateScore(score):
	highscore = max(highscore,score);
	get_node("Score/Label").set_text("Highscore: " + str(highscore));
