extends Node2D

class Obj:
	var delay;
	var type;
	var x;
	var spd;
	
	func _init(_delay=0,_type=Global.ObjType.NONE,_x=[0,1],_spd=2.5):
		delay = _delay;
		type = _type;
		x = _x;
		spd = _spd;

#onready = declare var here, but initialize it in the _ready() function automatically
onready var windowSize = Vector2(Globals.get("display/width"),Globals.get("display/height"));

var time = 0;
var prefab = load("res://FallingObject.tscn");
var level = [];

export(NodePath) var levelRoot;
	
func CreateWave(levelNum):
	# no switch/match statement AFAIK, going the old-fashioned way for now
	if levelNum==0:
		for i in range(5):
			level.push_back(Obj.new(0.4,Global.ObjType.COMPOSTABLE,[0,1]));
	if levelNum==1:
		for i in range(5):
			level.push_back(Obj.new(0.5,Global.ObjType.COMPOSTABLE,float(i)/4));
	elif levelNum==2:
		for i in range(5):
			level.push_back(Obj.new(0.3,Global.ObjType.NONCOMPOSTABLE,[0.2,0.3]));
			level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE,[0.7,0.8]));
	elif levelNum==3:
		for i in range(5):
			level.push_back(Obj.new(0.3,Global.ObjType.COMPOSTABLE,[0.2,0.3]));
			level.push_back(Obj.new(0,Global.ObjType.NONCOMPOSTABLE,[0.7,0.8]));
	elif levelNum==4:
		for i in range(4):
			level.push_back(Obj.new(0.3,Global.ObjType.NONCOMPOSTABLE,1-float(i)/4));
		level.push_back(Obj.new(0.3,Global.ObjType.COMPOSTABLE,0));
	elif levelNum==5:
		for i in range(4):
			level.push_back(Obj.new(0.3,Global.ObjType.NONCOMPOSTABLE,float(i)/4));
		level.push_back(Obj.new(0.3,Global.ObjType.COMPOSTABLE,1));
	elif levelNum==6:
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.25));
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.2));
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.15));
		level.push_back(Obj.new(0.5,Global.ObjType.COMPOSTABLE, 0.25));
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.2));
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.15));
		level.push_back(Obj.new(0.8,Global.ObjType.NONE));
		for i in range(4):
			level.push_back(Obj.new(0.2,Global.ObjType.NONCOMPOSTABLE, 0.5,4));
	elif levelNum==7:
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.75));
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.8));
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.85));
		level.push_back(Obj.new(0.5,Global.ObjType.COMPOSTABLE, 0.75));
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.8));
		level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE, 0.85));
		level.push_back(Obj.new(0.8,Global.ObjType.NONE));
		for i in range(4):
			level.push_back(Obj.new(0.2,Global.ObjType.NONCOMPOSTABLE, 0.5,4));
	elif levelNum==8:
		for i in range(5):
			level.push_back(Obj.new(0.4,Global.ObjType.COMPOSTABLE,[0,1]));
	elif levelNum==9:
		var hole = randi()%3+1
		for i in range(5):
			if (i>=hole-1 && i<=hole+1): level.push_back(Obj.new(0,Global.ObjType.COMPOSTABLE,float(i)/4));
			else: level.push_back(Obj.new(0,Global.ObjType.NONCOMPOSTABLE,float(i)/4));

func _process(dt):
	time += dt;
	if level.size() <= 0:
		time = time - 0.5;
		CreateWave(randi()%9);
	else: 
		while (level.size() > 0 && time > level[0].delay):
			time = time - level[0].delay;
			CreateObject(level[0].type,level[0].x,level[0].spd);
			level.pop_front();

func CreateObject(type,x=[0,1],spd=2.5,y=1):
	if (type == Global.ObjType.NONE): return;
	
	var obj = prefab.instance();
	add_child(obj);
	var xPos = GetRange(x);
	var yPos = 1-GetRange(y);
	obj.set_pos(Vector2(lerp(xPos,0.5,0.15)*windowSize.x,yPos*windowSize.y));
	obj.spd = GetRange(spd);
	obj.SetType(type);
	obj.connect("ObjectDestroyed",get_node(levelRoot),"ObjectDestroyed");
	
func GetRange(obj):
	if typeof(obj) >= TYPE_ARRAY:
		return randf()*(obj[1]-obj[0]) + obj[0];
	else:
		return obj;
