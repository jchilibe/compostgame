extends Node2D

#var compost = 0;
#var maxCompost = 10;
var baseScale;

func _ready():
	# connect signal to trigger OnCollide when condition is met
	baseScale = get_node("Image").get_scale().x;
	connect("body_enter", self, "OnCollide");
	set_fixed_process(true);
	pass

func OnCollide(obj):
	# on hitting something, gain compost and destroy the something
	if (obj.type <= Global.ObjType.NONCOMPOSTABLE):
			get_node("Image").set_scale(Vector2(baseScale*1.25,baseScale*1.25));
	obj.Destroy(true);

func _fixed_process(delta):
	var mouseX = get_viewport().get_mouse_pos().x;
	
	var dist = get_pos().x - mouseX;
	get_node("Image").set_rot(lerp(get_node("Image").get_rot(),-dist*PI/360,0.2));
	var newScale = lerp(get_node("Image").get_scale().x,baseScale,0.2);
	get_node("Image").set_scale(Vector2(newScale,newScale));
	
	set_pos(Vector2(mouseX,get_pos().y));
	
#func GainCompost():
#	compost = min(maxCompost,compost+1);
#	get_node("Compost").set_scale(Vector2(1,float(compost)/maxCompost));