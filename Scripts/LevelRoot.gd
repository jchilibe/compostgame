extends Node

onready var spawner = get_node("Spawner");
onready var cow = get_node("Cow");
onready var infoPanel = get_node("InfoPanel");

signal GameOverUpdateScore(score);

var lives = 3;
var score = 0;

func _ready():
	randomize();
	
	spawner.CreateWave(0);
	UpdateScoreText();
	
	#startButton.connect("pressed",self,"StartGame");
	spawner.set_process(true);
	pass
	
#Called by signal whenever a falling object is removed
func ObjectDestroyed(collected,compostable):
	if collected:
		if compostable: score = score+1;
		else: lives = lives-1;
	else:
		if compostable: lives = lives-1;
	UpdateScoreText();
	
func UpdateScoreText():
	if lives > 0: infoPanel.get_node("Label").set_text("Lives: " + str(lives) + "     Score: " + str(score));
	else:
		infoPanel.get_node("Label").set_text("GAME OVER - Final Score: " + str(score));
		ClearLevel();
	
func ClearLevel():
	emit_signal("GameOverUpdateScore",score);
	get_node("Spawner").queue_free();
	get_node("GameOverPanel").show();
	get_node("GameOverPanel/MenuBtn").connect("pressed",self,"DeleteSelf");
	
func DeleteSelf():
	queue_free();
	#get_node("Cow").queue_free();
	
	