extends KinematicBody2D

onready var windowSize = Vector2(Globals.get("display/width"),Globals.get("display/height"));

var type = 1;
var spd = 2 # in tenths of the screen per second

signal ObjectDestroyed(collected,compostable);

func _ready():
	set_fixed_process(true);
	pass
	
func SetType(t):
	type = t;
	get_node("Sprite").set_texture(load("res://Sprites/Object000" + str(type) + ".png"));
	
func Destroy(collected=true):
	# check if properly disposed of (collected compost or avoided non-compost)
	emit_signal("ObjectDestroyed",collected,type < Global.NONCOMPOSTABLE);
	# destroy this object
	hide();
	queue_free();
	
func _fixed_process(delta):
	move(Vector2(0,delta*(windowSize.y/10)*spd));
	if (get_pos().y > windowSize.y):
		Destroy(false);
